import 'react-native-gesture-handler';

import React, { useState } from 'react';

import AppNavigator from './navigation/index';
import { Provider } from 'react-redux';
import {init} from './db';
import store from './store/index';
import { useFonts } from 'expo-font';

init()
  .then(()=>{
    console.log("BD inicializada")
})
.catch((err)=>{
  console.log("BD inicializada con errores", err);
});

export default function App() {
  const [textCategoria, setTextCategoria] = useState();
  const [loaded] = useFonts({
     'Bebas': require('../assets/fonts/BebasNeue-Regular.ttf'),
  });

  const onSelectItem = (item) => {
    setTextCategoria(item);
  };
  
  if (!loaded) {
    return null;
  }

  
  return (
    <Provider store = {store}>
        <AppNavigator />
    </Provider>
    
  );
}

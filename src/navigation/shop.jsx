import PrimeraVista  from "../screens/first/index";
import React from "react";
import SegundaVista from "../screens/second/index";
import { StackRouter } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

const ShopNavigator = () => {
    return (
        <Stack.Navigator initialRouteName="primera">
            <Stack.Screen name="primera" component={PrimeraVista} options={{
                headerShown: false
            }}/>
            <Stack.Screen name="segunda" component={SegundaVista} options={{
                headerShown: false
            }}/>
        </Stack.Navigator>

    );
};

export default ShopNavigator;
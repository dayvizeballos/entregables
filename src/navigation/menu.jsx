import ContactoVista from '../screens/contacto/index';
import React from "react";
import ShopNavigator  from "./shop";
import { createDrawerNavigator } from "@react-navigation/drawer";

const Menu = createDrawerNavigator();

const MenuNavigator = () => {
    return (
            <Menu.Navigator initialRouteName = "shopnavigator" >
                <Menu.Screen name="shopnavigator" component={ShopNavigator} options={{
                    title: 'Clasificación'
                }}/>
                <Menu.Screen name="contacto" component={ContactoVista} options={{
                    title: 'Contacto'
                }}/>
            </Menu.Navigator>
    );
};

export default MenuNavigator;
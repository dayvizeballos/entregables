import AuthScreen  from "../screens/auth/index";
import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

const AuthNavigator = () => {
    return (
        <Stack.Navigator 
            initialRouteName="Auth"
            screenListeners={{
                headerShown : false
            }}>
            <Stack.Screen name="Auth" component={AuthScreen}/>
        </Stack.Navigator>

    );
};

export default AuthNavigator;
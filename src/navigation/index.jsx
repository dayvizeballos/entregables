import AuthScreen from "../screens/auth";
import MenuNavigator from "./menu"
import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { useSelector } from "react-redux";

const AppNavigator = () => {
    const userId = useSelector((state) => state.auth.userId);

    return (
        <NavigationContainer>
            {userId ? <MenuNavigator /> : <AuthScreen/>}
        </NavigationContainer>
    );
};

export default AppNavigator;

import { StyleSheet } from "react-native";
import colors from '../../constants/colors';

export const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    input: {
        width: '80%',
        borderBottomWidth: 1,
        borderBottomColor: '#7D8CC4',
    },
    label:{
        marginVertical:20,
    },
    button: {
        width: '80%',
        backgroundColor: colors.confirma,
        marginVertical: 20,
    }
});
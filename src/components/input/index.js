import { Button, Text, TextInput, View } from "react-native";

import React from "react";
import colors from "../../constants/colors";
import { styles } from "./styles";

const CustomInput = ({item, onChangeText, placeholder, onPressButton, inputValue, buttonText}) => {
    return (
    <View style={styles.inputContainer}>
      <Text style={styles.label}>Nueva categoria</Text>
      <TextInput 
        style={styles.input}
        placeholderTextColor='#cccccc'
        placeholder={placeholder}
        value={item}
        onChangeText={onChangeText}
      />
      <View style={styles.button}>
        <Button title={buttonText} color='#fff' onPress={onPressButton} disabled={inputValue === ''}/>
      </View>
     </View>
    )
}

export default CustomInput;
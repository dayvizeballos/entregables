import { Dimensions, StyleSheet } from "react-native";

const { width, height } = Dimensions.get('window');

export const styles = StyleSheet.create({
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10,
        backgroundColor: '#FFF',
        paddingHorizontal:10,
        borderRadius: 7,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    item: {
        fontSize: 18,
        fontFamily: 'Bebas',
    },
    image: {
        width: width * 0.10,
        height: height * 0.10,
      },
    touch: {
        flex: 1,
        flexDirection: 'row'
    }
});
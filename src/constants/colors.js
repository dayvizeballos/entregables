export default {
  confirma: '#242038',
  primary : '#7C77B9',
  white: '#ffffff',
  black: '#000000',
  gray: '#f5f5f5',
  placeholder: '#a0a0a0'
};

//inicializar STORE
import { applyMiddleware, combineReducers, createStore } from "redux";

import authReducer from './reducers/auth.reducers';
import categoryReducer from './reducers/category.reducers';
import thunk from "redux-thunk";

const rootReducer = combineReducers({
   category : categoryReducer,
   auth : authReducer, 
});

export default createStore(rootReducer, applyMiddleware(thunk));

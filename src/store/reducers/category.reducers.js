import {LISTA_CLASIFICACION} from '../../constants/clasificacion';
import { categoryTypes } from '../types';

const {SELECT_CATEGORY,MODIFY_CATEGORY,LISTAR_CATEGORY,ADD_CATEGORY,DEL_CATEGORY} = categoryTypes;

const initialState = {
    categories:[],
    selected : null
}

//reductores reciben un estado y una accion siempre
const categoryReducer = (state = initialState, action) => {
    switch(action.type){
        case SELECT_CATEGORY:
            const indexCategory = state.categories.findIndex(
                (category) => category.id === action.categoryId
            );
            if(indexCategory === -1) return state;
            return {
                ...state,
                selected: state.categories[indexCategory],
            }
        case MODIFY_CATEGORY:
            return state
        case LISTAR_CATEGORY:
            return {
                ...state,
                categories : action.resultado,
            };
        case ADD_CATEGORY:
            return state;
        
        case DEL_CATEGORY:
            return state;
          
        default:
            return state;
    }
}

export default categoryReducer;
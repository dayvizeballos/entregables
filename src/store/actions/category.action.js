import { URL_API } from "../../constants/firebase";
import { categoryTypes } from "../types";
import {insertarRegistro} from "../../db";

const {SELECT_CATEGORY, MODIFY_CATEGORY, ADD_CATEGORY, LISTAR_CATEGORY, DEL_CATEGORY} = categoryTypes;

export const selectCategory = (id) => ({
    type: SELECT_CATEGORY,
    categoryId : id
})

export const modifyCategory = (id,text) => {
    return async (dispatch) => {
        try{
            const response = await fetch(`${URL_API}/clasificacion/${id}.json`,{
                method : "PUT",
                headers : {
                    "Content-Type" : "application/json",
                },
                body : JSON.stringify({
                    date: Date.now(),
                    value: text,
                    img: '../../../assets/pantalon.jpg'
                }),
            });
            const result = await response.json();
            dispatch({
                type : MODIFY_CATEGORY,
                confirm : true,
            })
        }catch (error){
            console.warn(error);
        }
    }
};

export const listarCategory = () => {
    return async (dispatch) => {
        try{
            const response = await fetch(`${URL_API}/clasificacion.json`,{
                method : "GET",
                headers : {
                    "Content-Type" : "application/json",
                },
            });
            const data = await response.json();
            const resultado = Object.keys(data).map((key) => {
                return {
                    ...data[key],
                    id : key
                }
            })
            dispatch({
                type : LISTAR_CATEGORY,
                resultado
            })
        }catch (error){
            console.warn(error);
        }
    }
}

export const deleteCategory = (id) => {
    return async (dispatch) => {
        try{
            const response = await fetch(`${URL_API}/clasificacion/${id}.json`,{
                method : "DELETE",
                headers : {
                    "Content-Type" : "application/json",
                },
            });
            const result = await response.json();
            dispatch({
                type : DEL_CATEGORY,
                confirm : true,
            })
        }catch (error){
            console.warn(error);
        }
    }
}

//export const addCategory = (text, nextId) => 
export const addCategory = (text) => 
{
    return async (dispatch) => {
        try{
            const response = await fetch(`${URL_API}/clasificacion.json`,{
                method : "POST",
                headers:{
                    "Content-Type" : "application/json",
                },
                body : JSON.stringify({
                    date: Date.now(),
                    value: text,
                    img: '../../../assets/pantalon.jpg'
                }),
            });

            const result = await response.json();
            const resultId = await insertarRegistro(text);
            console.log(resultId);
            dispatch({
                type : ADD_CATEGORY,
                confirm: true,
            });
        }catch (error){
            console.warn(error);
        }
    }
}


//  ({
//      type : ADD_CATEGORY,
//      categoryText : text
//  })





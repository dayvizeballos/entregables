import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase('places.db');
export const init = () => {
    const promise = new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql(
                'CREATE TABLE IF NOT EXISTS tipo_vestimenta (id INTEGER PRIMARY KEY NOT NULL, nombre TEXT);',
                [],
                () => resolve(),
                (_,err) =>reject(err) 
            );
        });
    });
    return promise;
};

export const insertarRegistro = (nombre) => {
    const promise = new Promise((resolve, reject) => {
        db.transaction((tx) => {
                tx.executeSql(
                    'INSERT INTO tipo_vestimenta ( nombre) VALUES (?);',
                    [nombre],
                    (_,result) => {
                      resolve(result);
                    },
                    (_,err) =>reject(err)
                );
        });
    });
    return promise;
};

export const obtenerVestimenta = () => {
    const promise = new Promise((resolve, reject) => {
        db.transaction((tx) => {
            tx.executeSql(
                'SELECT * from tipo_vestimenta;',
                [],
                (_,result) => {
                    resolve(result);
                  },
                  (_,err) =>reject(err)
            );
        });
    });
    return promise;
};
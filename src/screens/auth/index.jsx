import {Button, KeyboardAvoidingView, StyleSheet, Tex, Text, TextInput, TouchableOpacity, View} from "react-native";
import React, {useState} from "react";
import {signIn, signUp} from '../../store/actions/auth.action';

import colors from '../../constants/colors';
import {styles} from "./styles";
import { useDispatch } from "react-redux";

const AuthScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [isLogin, setIsLogin] = useState(true);
    const [password, setPassword] = useState('');
    const title = isLogin ? 'Login' : 'Registro';
    const message = isLogin ? '¿No tienes una cuenta?' : '¿Ya tienes una cuenta?'
    const messageAction = 'Registrate';
    const messageTarget = isLogin ? 'Ingresar' : 'Registrate';
    const onHandleAuth = () => {
        dispatch(isLogin ? signIn(email,password) : signUp(email,password));
    };
    const onHandleChange = (value , type) => {
        if(type==='email'){
            setEmail(value);
        }else{
            setPassword(value);
        }
    };

    const onHandleChangeAuth = () => {
        setEmail("");
        setPassword("");
        setIsLogin(!isLogin)
    }

    return (
        <KeyboardAvoidingView
            style={styles.containerKeyboard} behavior="padding">
            <View style={styles.container}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.label}>Correo electrónico</Text>
                <TextInput 
                    style={styles.input}
                    placeholder='Ingrese su email'
                    placeholderTextColor={colors.placeholder}
                    autoCapitalize = "none"
                    autoCorrect = {false}
                    keyboardType = "email-address"
                    onChangeText={(text) => onHandleChange(text,'email')} 
                    value = {email}/>

                <Text style={styles.label}>Contraseña</Text>
                <TextInput 
                    style={styles.input}
                    placeholder='Ingrese su contraseña'
                    placeholderTextColor={colors.placeholder}
                    autoCapitalize = "none"
                    autoCorrect = {false}
                    secureTextEntry
                    onChangeText={(text) => onHandleChange(text,'password')}
                    value = {password} />

                <Button 
                    title = {messageTarget}
                    color = {colors.primary}    
                    onPress = {onHandleAuth}
                    disabled = {!(email && password)}
                        />
                        
                <View style={styles.prompt}>
                    
                    <TouchableOpacity onPress={onHandleChangeAuth}>
                        <Text style={styles.promptMessage}>{message}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </KeyboardAvoidingView>
    );
}

export default AuthScreen;
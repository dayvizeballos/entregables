import { StyleSheet } from "react-native";
import colors from '../../constants/colors';

export const styles = StyleSheet.create({
    containerKeyboard:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
    container:{
        maxWidth: 400,
        width: '80%',
        padding: 15,
        margin:15,
        borderColor: colors.primary,
        borderWidth: 1,
        borderRadius: 10,
        backgroundColor: colors.white,
    },
     title:{
         fontSize: 18,
         marginBottom: 15,
         textAlign: "center",
     },
     label:{
         fontSize: 14,
         marginVertical: 5
     },
     input:{
         height: 45,
         borderBottomColor: colors.primary,
         borderBottomWidth: 1,
         width: "90%",
         marginBottom: 10,
     },
      prompt:{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
      },
      promptMessage:{
          fontSize: 14,
          color: "black",
          marginRight: 15,
      },
  });
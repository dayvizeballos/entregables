import {StyleSheet, Text, View} from "react-native";

import {styles} from "./styles";

const ContactoVista = () => {

    return (
        <View
            styles={styles.container}>
            <Text>Contacto</Text>
        </View>
    );
}

export default ContactoVista;
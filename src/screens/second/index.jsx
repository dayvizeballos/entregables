import { Button, Text, TextInput, View } from 'react-native';
import React, { useState } from 'react';
import {deleteCategory, modifyCategory} from '../../store/actions/category.action';
import { useDispatch, useSelector } from 'react-redux';

import { styles } from "./styles";

const SegundaVista = ({navigation, route}) => {
    const dispatch = useDispatch();
    const [item, setItem] = useState('');
    const categorySelected = useSelector((state) => state.category.selected);
    const inputvalue = item.trim();
    
    const onModify = () => {
      dispatch(modifyCategory(categorySelected.id, item));
      navigation.navigate("primera",{
        modificado : true
      });
    }

    const onDelete = () => {
      dispatch(deleteCategory(categorySelected.id));
      navigation.navigate("primera",{
        modificado : true
      });
    }

    const onChangeText = (text) => {
      setItem(text)
    }
    
    return (
        <View style={styles.container}>
          <View style={styles.containerGeneral}>

            <View style={styles.inputContainer}>
                <Text style={styles.label}>Modificar categoria</Text>
                <TextInput 
                    item={item}
                    style={styles.input}
                    placeholderTextColor='#cccccc'
                    placeholder='Modificar categoria'
                    onChangeText={onChangeText}
                    defaultValue={categorySelected.value}
                    inputValue={inputvalue}
                />
                <View style={styles.button}>
                    <Button title='Modificar' color='#fff' onPress={() => onModify()}/>
                </View>
               
                <Button title="Regresar a la lista" onPress={() => navigation.navigate("primera")} color="#1493e2" />

                <View style={styles.button}>
                    <Button title='Eliminar' color='#fff' onPress={() => onDelete()}/>
                </View>
            </View>

         </View>
        
        </View>
      );
};

export default SegundaVista;
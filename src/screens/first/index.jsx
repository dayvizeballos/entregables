import { Button, FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import { CustomInput, CustomModal, ListaItems } from '../../components/index';
import React, { useEffect, useState } from 'react';
import {addCategory, listarCategory, selectCategory} from '../../store/actions/category.action';
import { useDispatch, useSelector } from 'react-redux';

import {LISTA_CLASIFICACION} from "../../constants/clasificacion";
import close  from '../../../assets/cerrar.png';
import { styles } from "./styles";
import { useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';

const PrimeraVista = ({navigation, route}) => {
      const dispatch = useDispatch();
      const categories = useSelector((state) => state.category.categories);
      const categorySelected = useSelector((state) => state.category.selected);
      const [itemList, setItemList] = useState(LISTA_CLASIFICACION);
      const [modalVisible, setModalVisible] = useState(false);
      const [itemSelected, setItemSelected] = useState({});
      const [item, setItem] = useState('');
      const inputvalue = item.trim();
      const renderItem = ({ item }) => (
        <ListaItems item={item}  onHandlerModal={onHandlerModal} onSelected={onSelected} onCambio={onCambio}/>
      )

      useFocusEffect(
        useCallback(()=>{
          dispatch(listarCategory());
      }, [dispatch])
      );
      
      const onChangeText = (text) => {
        setItem(text)
      }

      const onCambio = (id, text) =>{
       
      }

      const onSelected = (item) => {
        dispatch(selectCategory(item.id));
        navigation.navigate("segunda",{
          objeto : item,
        });
      };
      
      const onAdd= () => {
        setModalVisible(!modalVisible);
      };
    
      const addItem = () => {
        // let max = 0;
        // if(categories && categories?.length > 0){
        //   max = categories.reduce(function(prev, current) {
        //     return (prev.id > current.id) ? prev.id : current.id
        //   }) 
        // }
        if (inputvalue) {
          //dispatch(addCategory(item,(+max+1)+''));
          dispatch(addCategory(item));
          setItem('');
          setModalVisible(!modalVisible);
        }
        dispatch(listarCategory());
      }
      
      const onHandlerModal = (id) => {
        setItemSelected(itemList.find(item => item.id === id));
      }

      return (
        <View style={styles.container}>
          <View style={styles.containerGeneral}>
            
          <View style={styles.buttonContainer}>
              <Button title="Agregar Clasificacion" onPress={() => onAdd()} color="#fff" />
          </View>
              
          <Text style={styles.message} >1. Clic en algun elemento para modificar y pasar a la siguiente pantalla"</Text>
          <Text style={styles.message} >2. Puede agregar mas elementos"</Text>
            
            <View style={styles.itemList}>
              <FlatList 
                data={categories}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
              />
            </View>
         </View>
         <CustomModal animationType='fade' modalVisible={modalVisible}>
            <View style={styles.modal}>
              <View style={styles.modalContent}>
                <View style={styles.modalBox}>
                   <TouchableOpacity
                      onPress={()=>  setModalVisible(!modalVisible)}>
                    <Image source={close} />
                  </TouchableOpacity>
                  
                </View>
                <View>
                  <CustomInput 
                    item={item}
                    onChangeText={onChangeText}
                    placeholder='Escriba categoria'
                    onPressButton={addItem}
                    inputValue={inputvalue}
                    buttonText='Agregar'
                  />
                </View>
              </View>
            </View>
         </CustomModal>
        </View>
      );

};

export default PrimeraVista;